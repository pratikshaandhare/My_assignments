import Image
import numpy as np
import scipy.ndimage

im = Image.open('shivaji.jpg')
data = np.array(im)
threshold = 200
window = 10 # This is the "full" window...
new_value = 0

mask = data > threshold
mask = scipy.ndimage.uniform_filter(mask.astype(np.float), size=window)
mask = mask > 0
data[mask] = new_value
