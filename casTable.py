import Tkinter as tk
from Tkinter import *

def castaliaTable(master,data):
	tableFrame = Frame(master)
	r = len(data)
	c = len(data[0])
	
	rows = []
	for i in range(r):
		cols = []
		if i == 0:
			backgnd = 'yellow'
		else:
			backgnd = 'white'
		for j in range(c):
			e = Label(tableFrame,relief=RIDGE,bg=backgnd,width=10)
			e.grid(row=i, column=j, sticky=NSEW)
			#~ e.insert(END, data[i][j])
			e['text']=data[i][j]
			cols.append(e)
		rows.append(cols)
		tableFrame.place(x= 100,y=100)

	def onPress():
		for row in rows:
			for col in row:
				print col.get(),
			print


root = tk.Tk()
root.withdraw()
master = tk.Toplevel()
screenWidth=master.winfo_screenwidth()
screenHeight=master.winfo_screenheight()
master.geometry(("%dx%d")%(screenWidth,screenHeight))

data = 	 [['First','Second','Third'],[1,2,3],[3,4,5]]
castaliaTable(master,data)
mainloop()
