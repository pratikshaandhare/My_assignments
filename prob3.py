#Add,subtract images layers familiar with the data types.

import numpy as np
import cv2
import matplotlib.pyplot as plt

img = cv2.imread('shivaji.jpg')
r = img[:,:,0]
g = img[:,:,1]
b = img[:,:,2] 
a=r+g
b=g+b
c=r+b
d=r-g
e=g-b
f=r-b
cv2.imshow('image1', a)
cv2.imshow('image2', b)
cv2.imshow('image3', c)
cv2.imshow('image4', d)
cv2.imshow('image5', e)
cv2.imshow('image6', f)
cv2.waitKey(0)