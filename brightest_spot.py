# find brightest spot in an image
import cv2
import numpy as np
im=cv2.imread('shivaji.jpg')
g = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
a = np.array(g)
print a.max(), np.unravel_index(a.argmax(), a.shape)
# the area of the image with the largest intensity value
(minVal, maxVal, minLoc, maxLoc) = cv2.minMaxLoc(g)
cv2.circle(g, maxLoc, 16, (255, 0, 0), 3)
 
# display the results 
cv2.imshow("image", g)
cv2.waitKey(0)
