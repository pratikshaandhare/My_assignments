#Read a grayscale image and rotate to (30,45,90) angles,scale to 128X128,512X512 and crop to 50X50 pixels from center of the image.
import numpy as np
import cv2
import matplotlib.pyplot as plt
image = cv2.imread("shivajigray.jpeg")

(h, w) = image.shape[:2]
center = (w / 2, h / 2) 
# rotate the image
M1 = cv2.getRotationMatrix2D(center, 30, 1.0)
rotated1 = cv2.warpAffine(image, M1, (w, h))
M2 = cv2.getRotationMatrix2D(center, 45, 1.0)
rotated2 = cv2.warpAffine(image, M2, (w, h))
M3 = cv2.getRotationMatrix2D(center, 90, 1.0)
rotated3 = cv2.warpAffine(image, M3, (w, h))


# perform resizing 
resized = cv2.resize(image, (128,128), interpolation = cv2.INTER_AREA)
#crop image
cropped = image[50:h/2, 50:w/2]

plt.subplot(331), plt.imshow(rotated1,'gray')
plt.subplot(332), plt.imshow(rotated2,'gray')
plt.subplot(333), plt.imshow(rotated3,'gray')
plt.subplot(334), plt.imshow(resized,'gray')
plt.subplot(335), plt.imshow(cropped,'gray')
plt.show()