import numpy as np
import cv2
import matplotlib.pyplot as plt
plt.ion()


pts = np.array([[100,50],[50,100],[100,150],[50,200],[100,250],[200,250],[250,200],[200,150],[250,100],[200,50],[100,50]], np.int32)

plt.axis([0,500,0,500])
plt.plot(pts[:,0],pts[:,1],'bo')
plt.plot(pts[:,0],pts[:,1],'b')




def Translation(points,tX=0,tY=0):
	"""Input:points= matrix(size Nx2) ,tX =translation in X,tY = translation in Y 
	Output:new translated points(Nx3)"""
	if points.shape[1]==2:
		pointMatrix = np.ones([3,points.shape[0]])
		pointMatrix[0:2,:] = points.transpose()
		transMatrix = np.mat([[1,0,tX],[0,1,tY],[0,0,1]])
		outputPoints = transMatrix*pointMatrix
	else:
		print "Invalid input enter points in the format Nx2"
		return -1
	return outputPoints
	
points = Translation(pts,10)
pts = points[0:2,:].transpose()
plt.plot(pts[:,0],pts[:,1],'ro')
plt.plot(pts[:,0],pts[:,1],'r')
