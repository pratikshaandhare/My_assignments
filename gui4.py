import cv2
import Tkinter as tk
from Tkinter import *
from PIL import Image, ImageTk
from ttk import Frame, Button, Style
from Tkinter import RIGHT, BOTH, RAISED,Frame,Canvas
import numpy as np


class Gui():
	
	def __init__(self):
		self.cap = None
		self.status = None
		self.Master()
		
	def Master(self):
		self.master = tk.Tk()
		widthOfWindow = self.master.winfo_screenwidth()
		heightOfWindow = self.master.winfo_screenheight()
		self.master.geometry(str(widthOfWindow) + "x" + str(heightOfWindow))
		self.master.title("My Gui")
		label1 = tk.Label(self.master,bg = "black",fg = "white",text="My Project",font = "Verdana 16 bold")
		label1.place(x= 620,y = 10)
				
	def CVCapture(self):
		if self.cap==None:
			self.cap = cv2.VideoCapture(0)
		for i in range (2):
			ret, frame = self.cap.read()	
		return frame
		
	def StartButton(self):
		self.status=True
		self.Open(self.status)
	def StopButton(self):
		self.status=None
		print "status changed"
					
	def Open(self,status):
		CamPhoto = self.CVCapture()
		#~ bwImage = np.uint8(CamPhoto>120)*255
		cap1 = Image.fromarray(CamPhoto)
		show = ImageTk.PhotoImage(cap1)
		
		label = tk.Label(self.master, image = show)
		label.image = show
		label.place(x= 10,y = 50)
		self.master.update()
		print "before if"
		if self.status == True:
			print "after if"
			
			self.Open(status)
		else:
			print "Camera Stopped"
		
		
	def bton(self):
		start_img= Image.open("start.jpeg")
		start_img = ImageTk.PhotoImage(start_img)
		self.start_img = start_img
		startButton = tk.Button(master = self.master,text="Start",image =self.start_img,compound = 'top', command = self.StartButton)
		startButton.place(x=50, y=600)
		
		stop_img= Image.open("stop.png")
		stop_img = ImageTk.PhotoImage(stop_img)
		self.stop_img = stop_img
		stopButton = tk.Button(master = self.master,text="Stop",image =self.stop_img,compound = 'top', command = self.StopButton)
		stopButton.place(x=200, y=600)
		
		quit_img= Image.open("stop.png")
		quit_img = ImageTk.PhotoImage(quit_img)
		self.quit_img = quit_img
		quitButton = tk.Button(master = self.master,text="Quit",image =self.stop_img,compound = 'top', command = self.master.destroy)
		quitButton.place(x=1150, y=600)
		
	def Table(self,master,data):
		tableFrame = Frame(master)
		r = len(data)
		c = len(data[0])
		
		rows = []
		for i in range(r):
			cols = []
			if i == 0:
				backgnd = 'yellow'
			else:
				backgnd = 'white'
			for j in range(c):
				e = Label(tableFrame,relief=RIDGE,bg=backgnd,width=10)
				e.grid(row=i, column=j, sticky=NSEW)
				#~ e.insert(END, data[i][j])
				e['text']=data[i][j]
				cols.append(e)
			rows.append(cols)
			tableFrame.place(x= 100,y=100)
		data = 	 [['First','Second','Third'],[1,2,3],[3,4,5]]

		
		
guiObj = Gui()
guiObj.bton()
guiObj.Table(master,data)


